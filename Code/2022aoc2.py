with open('C:/Users/Adarsh/Downloads/Python/2022aoc2.txt') as data:
    lines = data.readlines()
    lines_array = [line.strip().split() for line in lines]

horizontal = 0
depth = 0

for item in lines_array:
    direction = item[0]
    increase = int(item[1])

    if direction == 'forward':
        horizontal += increase
    elif direction == 'down':
        depth += increase
    elif direction == 'up':
        depth -= increase

print(horizontal*depth)

# Part2
horizontal = 0
depth = 0
aim = 1
i = 0

for item in lines_array:
    direction  = item[0]
    increase = int(item[1])
    
    if i == 0:
        horizontal += increase
        aim = 0
    elif direction == 'forward':
        horizontal += increase
        current_depth = aim * increase
        depth += current_depth
        pass
    elif direction == 'down':
        aim += increase
    elif direction == 'up':
        aim -= increase
    i += 1


print(horizontal*depth)

    

        
            